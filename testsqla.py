from sqlalchemy import create_engine

# Create a database with `create_engine`:
engine = create_engine('sqlite:///testsqla.db', echo=True)

# `connect()` creates and returns a connection
# `execute()` execute SQL statements
# `begin()` establish a connection ready for transaction and returns a context manager
# `dispose()` closes all currently checked in db connections
# `driver` returns the db adapter used
# `table_names()` return all tables in the database
# `transaction` Executes the given function within a transaction boundary
