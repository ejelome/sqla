sqla
====

Introduction to SQLAlchemy

-------------------------------------------------------------------------------

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [sqla](#sqla)
    - [TBC](#tbc)
    - [SQLAlchemy is ...](#sqlalchemy-is-)
    - [Topics](#topics)
    - [Overview](#overview)
    - [Specifics](#specifics)
        - [Level 1](#level-1)
            - [DBAPI in a Nutshell](#dbapi-in-a-nutshell)
            - [SQLAlchemy and DBAPI](#sqlalchemy-and-dbapi)
                - [Create an Engine](#create-an-engine)
                    - [Multiple Transactions](#multiple-transactions)
    - [LICENSE](#license)

<!-- markdown-toc end -->

-------------------------------------------------------------------------------

TBC
---

- [SQLAlchemy Core - Connecting to Database](https://www.tutorialspoint.com/sqlalchemy/sqlalchemy_core_connecting_to_database.htm)
- [Introduction to SQLAlchemy](https://youtu.be/woKYyhLCcnU?t=2220)

-------------------------------------------------------------------------------

SQLAlchemy is ...
-----------------

- called an SQL toolkit
- an Object Relational Mapper (ORM)
- written in Python
- open-source
- cross platform
- released under MIT
- Implements [Data mapper pattern](https://en.wikipedia.org/wiki/Data_mapper_pattern)
  - Not to be confused with [Active record pattern](https://en.wikipedia.org/wiki/Active_record_pattern)

-------------------------------------------------------------------------------

Topics
------

- Tables, columns
- Data Definition Language (DDL)
- INSERT, UPDATE, DELETE (DML)
- SELECT, joins, grouping
- Overview of ACID

-------------------------------------------------------------------------------

Overview
--------

- ORM
  -  Object Relational Mapper (ORM) (construct and map Python objects to DB tables)
- Core
  - Schema / Types (uses Python objects to represent tables, columns, and datatypes)
  - SQL Expression Language (allows SQL statements to be written using Python expressions)
  - Engine (a registry providing connection to a specific DB)
    - Connection Pooling (holds collection of DB connections in-memory for fast re-use)
    - Dialect (interprets generic SQL and DB commands from specific DBAPI and DB backend)
- Third-party libraries / Python core
  - DBAPI
- Database

-------------------------------------------------------------------------------

Specifics
---------

### Level 1 ###

- Engine
  - Main job is to deal with Python DBAPI (PEP-0249)
- Connections
- Transactions

#### DBAPI in a Nutshell ####

1. Import DB adapter (e.g. psycopg2)
2. Establish connection (psycopg2.connect)
3. Bind connection, execute SQL expressions and retrieve results (psycopg2.cursor)
4. Close (then commit for non-select queries)
   - DBAPI doesn't auto commit
   - Transactions stays until you commit

#### SQLAlchemy and DBAPI ####

- The first layer
- Called the **Engine**
- The Object that maintains the classical DBAPI interaction

##### Create an Engine #####

``` python
from sqlalchemy import create_engine

# engine = create_engine('<path>') # general format
engine = create_engine('sqlite:///dbname.db') # relative path
# engine = create_engine('postgresql:////user:pass#@domain/dbname) # absolute path
# engine = create_engine('postgresql+psycopg2:////user:pass#@domain/dbname) # absolute path with adapter

result = engine.execute(
    """
    SELECT
        column
    FROM
       table
    WHERE
       id = :id
    """,
    id=1
)

row = result.fetchone()
result.close()
```

- Creates the database
- Relative path is three slashes (`///`)
- Absolute path is four slashes (`////`)
- The keyword before the colon (`:`) defines the dialect (e.g. `postgresql`)
  - An optional adapter can be specified prefixed with a plus (`+`) (e.g. `+psycopg2`)
    - `psycopg2` is the default
- `execute` accepts SQL statements then goes straight to the DBAPI
  - SQLAlchemy autocommits (insert, update and delete all autocommits)
- `fetch*()` returns a tuple-like pair of values `(<pk>, <value>)`
  - Results can be accessed like a dict (`row['column']`) or with dot notation (`row.column`)
- `close` closes the connection
  - Getting all the rows of results automatically closes the connection
  - Releases the DBAPI cursor and connection back to connection pool
  - Once closed, it's not anymore possible to access any results

###### Multiple Transactions ######

- Manual opening and closing transactions:

  ``` python
  conn = engine.connect()
  trans = conn.begin() # begin the transaction

  # Execute multiple transactions from here:
  conn.execute("<SQL>")
  conn.execute("<SQL>")

  trans.commit() # commit the transactions
  # trans.rollback() # don't apply the transactions
  conn.close()
  ```

- Automatic opening and closing transactions using `with`:

  ``` python
  with engine.begin() as conn:
    conn.execute("<SQL>")
    conn.execute("<SQL>")
  ```

-------------------------------------------------------------------------------

LICENSE
-------

`sqla` is licensed under [MIT](./LICENSE).
